extends Node

const MusicData: Dictionary = {
	'Battle of the Dragons': {
		'path' : "res://Data/Audio/OpeningSoundtracks/battle-of-the-dragons-8037.mp3"
	},
	'Epic Dramatic': {
		'path' : "res://Data/Audio/OpeningSoundtracks/epic-dramatic-action-trailer-99525.mp3"
	},
	'Stylish Rock': {
		'path' : "res://Data/Audio/OpeningSoundtracks/stylish-rock-beat-trailer-116346.mp3"
	}
}

onready var Soundtracks = MusicData.keys()

const VoiceData: Dictionary = {
	'Evil Ge': {
		'path' : "res://Data/Audio/Voices/evil_ge.mp3"
	},
	'Galaxy': {
		'path' : "res://Data/Audio/Voices/galaxy.mp3"
	},
	'Godot Open Source': {
		'path' : "res://Data/Audio/Voices/godot_open_source.mp3"
	},
	'Happy Potato': {
		'path' : "res://Data/Audio/Voices/happy_potato.mp3"
	},
	'Lets Go Shopping': {
		'path' : "res://Data/Audio/Voices/shopping.mp3"
	},
	'You Shall': {
		'path' : "res://Data/Audio/Voices/you_shall.mp3"
	}
}
