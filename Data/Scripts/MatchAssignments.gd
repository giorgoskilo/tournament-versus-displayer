extends Node2D

var player_matchups

func _ready():
	var window = get_node('Control/MusicAssignment')
	for j in range(MusicDatabase.Soundtracks.size()):
			window.get_node('OptionButton').add_item(MusicDatabase.Soundtracks[j])

func create_map(w, h):
	var map = []

	for x in range(w):
		var col = []
		col.resize(h)
		map.append(col)

	return map


func _on_Button_pressed():
	player_matchups = create_map(2,$Control/DuelsNumber.text)
	for i in range($Control/DuelsNumber.text):
		var window = get_node('Control/DuelAssignments/DuelAssignment' + str(i + 1))
		window.visible = true
		window.window_title = "Duel" + str(i + 1)
		for j in range(MatchupsInfo.players_number):
			window.get_node('Duelist1').add_item(MatchupsInfo.player_names[j])
			window.get_node('Duelist2').add_item(MatchupsInfo.player_names[j])


func _on_DUEL_pressed():
	MatchupsInfo.duels_number = int($Control/DuelsNumber.text)
	for i in range(MatchupsInfo.duels_number):
		var option1 = get_node('Control/DuelAssignments/DuelAssignment' + str(i + 1)).get_node('Duelist1')
		player_matchups[0][i] = option1.get_item_text(option1.selected)
		
		
		var option2 = get_node('Control/DuelAssignments/DuelAssignment' + str(i + 1)).get_node('Duelist2')
		player_matchups[1][i] = option1.get_item_text(option2.selected)
			
		MatchupsInfo.matchups_matrix = player_matchups
			 
	
	var music_selected = get_node('Control/MusicAssignment').get_node('OptionButton')
	MatchupsInfo.current_music = music_selected.get_item_text(music_selected.selected)
	 
	
	get_tree().change_scene("res://Data/Scenes/VersusDisplayer.tscn")


func _on_AssignMusic_pressed():
	var window = get_node('Control/MusicAssignment')
	window.visible = true
