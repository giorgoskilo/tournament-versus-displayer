extends Node

const Participants: Array = ['Bye', 'Godot Chad', 'Random G.E', 'Potato Head', 'Shopping G.', 'Super Boy',
'Wizard-O']

const PlayersData: Dictionary = {	
	'Godot Chad': {
		'real name': 'Godot Engine',
		'icon': 'godot_chad',
		'bg_skin': 'godot',
		'voice_skin' : 'Godot Open Source'
	},
	'Random G.E': {
		'real name': 'Game Engine',
		'icon': 'random_ge',
		'bg_skin': 'dual',
		'voice_skin' : 'Evil Ge'
	},
	'Potato Head': {
		'real name': 'Pat Potat',
		'icon': 'potato',
		'bg_skin': 'crisps',
		'voice_skin' : 'Happy Potato'
	},
	'Shopping G.': {
		'real name': 'Girl who shops',
		'icon': 'shopping_girl',
		'bg_skin': 'mall',
		'voice_skin' : 'Lets Go Shopping'
	},
	'Super Boy': {
		'real name': 'Jim jiminson',
		'icon': 'super_boy',
		'bg_skin': 'planets',
		'voice_skin' : 'Galaxy'
	},
	'Wizard-O': {
		'real name': 'Pandalf the Yellow',
		'icon': 'wizard',
		'bg_skin': 'magic',
		'voice_skin' : 'You Shall'
	},
	'Bye': {
		'real name': 'Bye',
		'icon': '',
		'bg_skin': '',
		'voice_skin' : ''
	}
}
