extends Node2D

onready var Player1 : Label = $Control/Player1
onready var Player2 : Label = $Control/Player2

onready var Character1 : TextureRect = $Control/Character1
onready var Character2 : TextureRect = $Control/Character2

onready var BGSkinLeft : TextureRect = $Control/VSMain/BGSkinLeft
onready var BGSkinRight : TextureRect = $Control/VSMain/BGSkinRight

onready var VoiceAudio : AudioStreamPlayer = $VoiceEffect

onready var Table : Label = $Control/Table

onready var animationPlayer : AnimationPlayer = $Control/AnimationPlayer

var tables = []
var random_index = []
var ready_flag = false
var finale_flag = false
var match_counter = 0

onready var matchups_matrix = MatchupsInfo.matchups_matrix
onready var duels_number = MatchupsInfo.duels_number

func _ready():
	OS.window_fullscreen = true
	
	$Control/DuelBackground.visible = false
	$Control/Duel.visible = false
	
	$AudioStreamPlayer.stream = ResourceLoader.load(MusicDatabase.MusicData[MatchupsInfo.current_music]['path'])
	$AudioStreamPlayer.play()
	
	tables.resize(duels_number) 
	for i in range(duels_number):
		tables[i] = i
	randomize()
	tables.shuffle()
	
	random_index.resize(duels_number) 
	for i in range(duels_number):
		random_index[i] = i
	randomize()
	random_index.shuffle()
	random_index.shuffle()
	
	assign_match(0)
	
	
func assign_match(i :int):
	if(i >= duels_number):
		final_act()
		return
		
	reset_sprites()
	
	Player1.set_text(matchups_matrix[0][random_index[i]]) 
	Player2.set_text(matchups_matrix[1][random_index[i]])

	#char and table assignment
	var char_texture1_name = PlayerDatabase.PlayersData[Player1.get_text()]['icon']
	if char_texture1_name != '':
		Character1.texture = ResourceLoader.load("res://Data/Characters/" + char_texture1_name + ".png")
	else: 
		Character1.texture = ResourceLoader.load("res://Data/Characters/default.png")

	var char_texture2_name = PlayerDatabase.PlayersData[Player2.get_text()]['icon']
	if char_texture2_name != '':
		Character2.texture = ResourceLoader.load("res://Data/Characters/" + char_texture2_name + ".png")
	else:
		Character2.texture = ResourceLoader.load("res://Data/Characters/default.png")

	Table.set_text("Table " + str(tables[i] + 1)) 
	
	
	#bg skin assignment
	var bg_skin_left = PlayerDatabase.PlayersData[Player1.get_text()]['bg_skin']
	if bg_skin_left != '':
		BGSkinLeft.texture = ResourceLoader.load("res://Data/BackgroundSkins/" + bg_skin_left + "_left.png")
		if BGSkinLeft.texture != null:
			BGSkinLeft.visible = true
		
	var bg_skin_right = PlayerDatabase.PlayersData[Player2.get_text()]['bg_skin']
	if bg_skin_right != '':
		BGSkinRight.texture = ResourceLoader.load("res://Data/BackgroundSkins/" + bg_skin_right + "_right.png")
		if BGSkinRight.texture != null:
			BGSkinRight.visible = true
			
	#voice assingment
	var voice_left = PlayerDatabase.PlayersData[Player1.get_text()]['voice_skin']
	if voice_left != '':
		VoiceAudio.stream = ResourceLoader.load(MusicDatabase.VoiceData[voice_left]['path'])
		VoiceAudio.play()
		$SoundFading.play('music_fade')
		$VoiceWait.set_wait_time(4.1)
		$VoiceWait.start()
	else:
		$VoiceWait.set_wait_time(0.2)
		$VoiceWait.start()


func final_act():
	animationPlayer.play('duel')
	$AudioStreamPlayer.stop()

func reset_sprites():
	BGSkinLeft.visible = false
	BGSkinLeft.texture = null
	BGSkinRight.visible = false
	BGSkinRight.texture = null
	
	VoiceAudio.stream = null


func _input(event):
	if event is InputEventKey && ready_flag:
		if event.pressed:
			assign_match(match_counter)
			animationPlayer.play()
			ready_flag = false			
			
		if event.scancode == KEY_Q:		
			get_tree().reload_current_scene()
			
	elif event is InputEventKey && finale_flag:
			get_tree().quit()


func _on_AnimationPlayer_animation_finished(anim_name):
	if(anim_name == "versus"):
		ready_flag = true
		match_counter += 1
	elif(anim_name == "duel"):
		finale_flag = true


func _on_VoiceWait_timeout():
	var voice_right = PlayerDatabase.PlayersData[Player2.get_text()]['voice_skin']
	if voice_right != '':
		VoiceAudio.stream = ResourceLoader.load(MusicDatabase.VoiceData[voice_right]['path'])
		VoiceAudio.play()
		$SoundFading.play('music_fade')
